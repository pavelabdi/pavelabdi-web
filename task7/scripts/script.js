[...document.getElementsByTagName("td")].map(el => {
    el.onmouseenter = function () {
        this.style.backgroundColor = "tomato";
    }
});

[...document.getElementsByTagName("td")].map(el => {
    el.onmouseout = function () {
        this.style.backgroundColor = "white";
    }
});

[...document.getElementsByClassName("nonselectable")].map(el => {
    el.onselectstart = function() {
        return false;
    }
});

[...document.getElementsByClassName("nonselectable")].map(el => {
    el.oncontextmenu = function() {
        return false;
    }
});

[...document.getElementsByClassName("flex-item")].map(el => {
    el.onmouseenter = function () {
        this.style.backgroundColor = "darkgrey";
    }
});

[...document.getElementsByClassName("flex-item")].map(el => {
    el.onmouseout = function () {
        this.style.backgroundColor = "lightgrey";
    }
});

clicks = 0;
document.getElementById("clickable").onclick = function () {
    clicks++;
    document.getElementById("like").innerHTML = "Like " + clicks;    
}

var tag;
var number = "";
var firstnumber;
var secondnumber;
var operator;
var result;
var display = "";
[...document.getElementsByClassName("flex-item")].map(el => {
    el.onclick = function () {
        tag = this.id;
        switch (tag) {
            case "0": number += tag; display += tag; document.getElementById("out").innerHTML = display; break;
            case "1": number += tag; display += tag; document.getElementById("out").innerHTML = display; break;
            case "2": number += tag; display += tag; document.getElementById("out").innerHTML = display; break;
            case "3": number += tag; display += tag; document.getElementById("out").innerHTML = display; break;
            case "4": number += tag; display += tag; document.getElementById("out").innerHTML = display; break;
            case "5": number += tag; display += tag; document.getElementById("out").innerHTML = display; break;
            case "6": number += tag; display += tag; document.getElementById("out").innerHTML = display; break;
            case "7": number += tag; display += tag; document.getElementById("out").innerHTML = display; break;
            case "8": number += tag; display += tag; document.getElementById("out").innerHTML = display; break;
            case "9": number += tag; display += tag; document.getElementById("out").innerHTML = display; break;
            case "=": {
                secondnumber = +number;
                number = "";
                switch(operator) {
                    case "+": result = firstnumber + secondnumber; break;
                    case "-": result = firstnumber - secondnumber; break;
                    case "*": result = firstnumber * secondnumber; break;
                    case "/": result = firstnumber / secondnumber; break;
                    default: result = "something's wrong";
                }
                display += " = " + result; 
                document.getElementById("out").innerHTML = display; 
                display = "";
                break;
            }
            default: {
                firstnumber = +number; 
                number = ""; 
                operator = tag;
                display += " " + tag + " ";
                document.getElementById("out").innerHTML = display;
            }
        }
    }
});


function myFunction(myDropdown) {
    document.getElementById(myDropdown).classList.toggle("show");
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.id != myDropdown) {
            openDropdown.classList.remove('show');
        }
    }
    console.log(myDropdown);
}

window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            openDropdown.classList.remove('show');
        }
    }
}

window.onscroll = function() {
    myScrollFunction()
}

function myScrollFunction() {
    if (window.pageYOffset > 100) {
        document.getElementById("arrow").classList.remove("sticky");
    } else {
        document.getElementById("arrow").classList.add("sticky");
    }
}

function myScrollTop() {
    document.documentElement.scrollTop = 0
}
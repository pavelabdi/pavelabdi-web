import React from 'react';
import App from './App';
import './App.css';

class App2 extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
      myList: {},
      number: 0,
      isEdit: false,
      idEdit: ''
    }
  }

  handleDelClick = (id) => {
    console.log(id)
    this.setState(
      (prevState) => {
        let newObj = {...prevState.myList}
        delete newObj[id]
        return {
          myList: newObj
        }
      }
    )
    console.log(this.state.myList)
  }

  handleEditClick = (id) => {
    console.log(this.state.myList)
    this.setState((prevState) => ({
        isEdit: true,
        idEdit: id,
        value: prevState.myList[id]
    }))
    console.log(this.state.idEdit)
  }

  handleKeyPress = (event) => {
    if(event.key === 'Enter'){
      this.handleClick()
    }
  }

  renderMyArray() {
    return (
      <>
      <input onChange={this.handleChange} value={this.state.value} onKeyPress={this.handleKeyPress}/>
      <button onClick={this.handleClick}>Enter</button>
      <App arr={this.state.myList} onClick={this.handleDelClick} onClickEdit={this.handleEditClick}/>
      </>        
    )
  }

  handleChange = (e) => {
    this.setState({
        value: e.target.value
    })
  }

  handleClick = () => {
    if (!this.state.isEdit) {
      this.setState(
        (prevState) => {
          let  newObj = {...prevState.myList}
          newObj[`${Date.now()}`] = prevState.value
          return {
            myList: newObj,
            number: prevState.number + 1,
            isEdit: false
          }
        }
      )} else {
      this.setState(
        (prevState) => {
          let newObj = {...prevState.myList}
          newObj[this.state.idEdit] = this.state.value
          return {
           myList: newObj,
            isEdit: false
          }
        }
      )
    }
    this.resetValue()
  }

  resetValue() {
    this.setState({
      value: ''
    })
  }

  render() {
    return (
    <>
      {this.renderMyArray()}
    </>
    )
  }
}

export default App2;
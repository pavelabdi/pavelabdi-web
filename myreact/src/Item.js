import React from 'react';
import './App.css';

class Item extends React.Component {
  
  render() {
    return (
      <div className="Flex">
        <li>
            {this.props.text}
        </li>
        <button onClick={() => {this.props.onClick(this.props.keyid)}}>delete</button>
        <button onClick={() => {this.props.onClickEdit(this.props.keyid)}}>edit</button>
      </div>
    )
  }
}

export default Item;

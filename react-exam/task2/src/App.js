import React from 'react';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
      isBlack: true
    }
  }

  handleKeyPress = (event) => {
    if(event.key === 'Enter'){
      this.handleClick()
    }
  }

  handleChange = (e) => {
    this.setState({
        value: e.target.value
    })
  }

  handleClick = () => {
    this.setState(prevState => ({
      isBlack: !prevState.isBlack
    }))
    
  }

  render() {
    return (
      <div className="Flex">
        <div className={this.state.isBlack ? 'Element LabelBlack' : 'Element LabelWhite'}>{this.state.value}</div>
        <input className="Element" onChange={this.handleChange} value={this.state.value}/>
        <button className="Element" onClick={this.handleClick}>Enter</button>
      </div>    
    )
  }
}

export default App;